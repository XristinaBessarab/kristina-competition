<?php
require ('../config.php');
require ('../common.php');

$new_user = array(
	"firstname" => escape($_POST['firstname']),
	"lastname"  => $_POST['lastname'],
	"email"     => $_POST['email']
);

print_r($new_user);
$insertSql = "insert into users (firstname, lastname, email) values ('{$new_user['firstname']}', '{$new_user['lastname']}', '{$new_user['email']}')";
echo $insertSql;

// nu met parameters
$insertSql = "insert into users (firstname, lastname, email) values (:firstname, :lastname, :email)";
echo $insertSql;

try {
    $connection = new \PDO($host, $user, $password, $options);
    
    $connection->exec('use Cursist9;');
    $statement = $connection->prepare($insertSql);
    $statement->execute($new_user);
} catch (\PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}

include ('template/header.php'); 
?>
<div id="feedback">
    <?php echo $new_user['firstname'] . ' is toegevoegd'; ?>
</div>
<!-- form>(label+input:text)*6 -->
<form action="" method="post">
    <div>
        <label for="firstname">Voornaam</label>
        <input type="text" name="firstname" id="firstname">
    </div>
    <div>
        <label for="lastname">Familienaam</label>
        <input type="text" name="lastname" id="lastname">
    </div>
    <div>
        <label for="email">E-mail</label>
        <input type="text" name="email" id="email">
    </div>
    <div>
        <label for="age">Leeftijd</label>
        <input type="text" name="age" id="age">
    </div>
    <div>
        <label for="location">Plaats</label>
        <input type="text" name="location" id="location">
    </div>
    <input type="submit" value="Verzenden" name="submit">
</form>
    
<?php include ('template/footer.php'); ?>