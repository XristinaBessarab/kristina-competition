<?php
require ('../../../config.php');
require ('../../../common.php');

if (isset($_POST['submit'])) {
    // anders dan Tania, wij escapen in het begin zodat
    // de waarde al 'veilig' is vooraleer naar de database te sturen
    $newUser = array(
    	"FirstName" => escape($_POST['FirstName']),
    	"LastName"  => escape($_POST['LastName']),
    	"Email"     => escape($_POST['Email']),
    	"Address1"  => escape($_POST['Address1']),
    	"Address2"  => escape($_POST['Address2']),
       	"PostalCode"  => escape($_POST['PostalCode']),
       	"City"  => escape($_POST['City']),
       	"Country"  => escape($_POST['Country']),
       	"Phone"  => escape($_POST['Phone']),
       	"Birthday"  => escape($_POST['Birthday'])
    );

    // volgende regel gebuik je op te debuggen (manuele breakpoint)
    // print_r($new_user);
    // niet veilige manier, gevoelig voor SQL injectie
    // $insertSql = "insert into users (firstname, lastname, email) values ('{$new_user['firstname']}', '{$new_user['lastname']}', '{$new_user['email']}')";
    // echo $insertSql;

    // nu met parameters, is veilig
    $insertSql = "insert into users (FirstName, LastName, Email, Address1, Address2, PostalCode, City, Country, Phone, Birthday) values (:FirstName, :LastName, :Email, :Address1, :Address2, :PostalCode, :City, :Country, :Phone, :Birthday)";
    // echo $insertSql;

    try {
        $connection = new \PDO($host, $user, $password, $options);
        $statement = $connection->prepare($insertSql);
        $statement->execute($newUser);
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }
}

include ('../../template/header.php'); 
?>
<div id="feedback">
<?php if (isset($_POST['submit']) && $statement) {
    echo $newUser['FirstName'] . ' ' . $newUser['LastName'] . ' is toegevoegd';
}
?>

</div>
<!-- form>(label+input:text)*6 -->
<form action="" method="post">
    <div>
        <label for="FirstName">Voornaam</label>
        <input type="text" name="FirstName" id="FirstName">
    </div>
    <div>
        <label for="LastName">Familienaam</label>
        <input type="text" name="LastName" id="LastName">
    </div>
    <div>
        <label for="email">E-mail</label>
        <input type="email" name="email" id="email">
    </div>
    <div>
        <label for="Address1">E-mail</label>
        <input type="text" name="Address1" id="Address1">
    </div>
    <div>
        <label for="Address2">E-mail</label>
        <input type="text" name="Address2" id="Address2">
    </div>
    <div>
        <label for="PostalCode">E-mail</label>
        <input type="text" name="PostalCode" id="PostalCode">
    </div>
    <div>
        <label for="City">Stad</label>
        <input type="text" name="City" id="City">
    </div>
     <div>
        <label for="Country">Land</label>
        <input type="text" name="Country" id="Country">
    </div>   
     <div>
        <label for="Phone">Tel</label>
        <input type="text" name="Phone" id="Phone">
    </div>
   <div>
        <label for="Birthday">Geboortedatum</label>
        <input type="text" name="Birthday" id="Birthday">
    </div>
 
    <input type="submit" value="Verzenden" name="submit">
</form>
    
<?php include ('template/footer.php'); ?>
