<?php
require ('../../../config.php');
require ('../../../common.php');

if (isset($_POST['submit'])) {
    // anders dan Tania, wij escapen in het begin zodat
    // de waarde al 'veilig' is vooraleer naar de database te sturen
    $newUser = array(
    	"Name" => escape($_POST['Name']),
    	"Year"  => escape($_POST['Year']),
    	"IsInPlanning" => escape($_POST['IsInPlanning'] == '0' ? false : true)
    );
    print_r($newUser);
    // volgende regel gebuik je op te debuggen (manuele breakpoint)
    // print_r($new_user);
    // niet veilige manier, gevoelig voor SQL injectie
    // $insertSql = "insert into users (firstname, lastname, email) values ('{$new_user['firstname']}', '{$new_user['lastname']}', '{$new_user['email']}')";
    // echo $insertSql;

    // nu met parameters, is veilig
    $insertSql = "insert into Liga (Name, Year, IsInPlanning) values (:Name, :Year, :IsInPlanning)";
    // echo $insertSql;

    try {
        $connection = new \PDO($host, $user, $password, $options);
        $statement = $connection->prepare($insertSql);
        $statement->execute($newUser);
    } catch (\PDOException $e) {
        echo "Er is iets fout gelopen: {$e->getMessage()}";
    }
}

include ('../../template/header.php'); 
?>
<div id="feedback">
<?php if (isset($_POST['submit']) && $statement) {
    echo $newUser['Name'] . ' is toegevoegd';
}
?>

</div>
<!-- form>(label+input:text)*6 -->
<form action="" method="post">
    <div>
        <label for="Name">Naam</label>
        <input type="text" name="Name" id="Name">
    </div>
    <div>
        <label for="Year">Jaar</label>
        <input type="text" name="Year" id="Year">
    </div>
    <div>
        <label for="IsInPlanning">Gepland?</label>
        <input type="radio" name="IsInPlanning" id="IsInPlanningYes" value="1"><label>Ja</label>
        <input type="radio" name="IsInPlanning" id="IsInPlanningNo" value="0"><label>Nee</label>
    </div>
    <input type="submit" value="Verzenden" name="submit">
</form>
    
<?php include ('../../template/footer.php'); ?>
